﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SecondMVCAppMy.DAL;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.Models.PostInTopics;
using SecondMVCAppMy.Models.Topics;
using SecondMVCAppMy.Services.PostInTopics.Contracts;

namespace SecondMVCAppMy.Services.PostInTopics
{
    public class PostInTopicService : IPostInTopicService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public PostInTopicService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }


        public PostInTopicSortAndPageModel GetPostInTopicSortPageModel(int topicId, PostInTopicSortAndPageModel model,
            PostInTopicSortState sortState)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {

                var topic = unitOfWork.Topics.GetByIdFullTopic(topicId);
                TopicModel topicModel = Mapper.Map<TopicModel>(topic);

                IEnumerable<PostInTopic> postInTopics = unitOfWork.PostInTopics.GetPostsWithAutorsAndTopicByTopics(topicId).ToList();
                List<PostInTopicModel> topicModels = Mapper.Map<List<PostInTopicModel>>(postInTopics);

                var postInTopicSortAndPageModel = topicModels.Sort(sortState);

                int pageSize = 3;
                int count = postInTopics.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                PostInTopicPageModel postInTopicPageModel = new PostInTopicPageModel(count, page, pageSize);
                postInTopicSortAndPageModel.PostInTopics = postInTopicSortAndPageModel.PostInTopics.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                postInTopicSortAndPageModel.PostInTopicSortState = sortState;
                postInTopicSortAndPageModel.PostInTopicPageModel = postInTopicPageModel;
                postInTopicSortAndPageModel.Page = page;
                postInTopicSortAndPageModel.TopicModel = topicModel;
                postInTopicSortAndPageModel.TopicModel.Id = topicId;

                return postInTopicSortAndPageModel;
            }
        }

        public List<PostInTopicModel> GetPostInTopicList(int topicId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var postInTopic = unitOfWork.PostInTopics.GetPostsWithAutorsAndTopicByTopics(topicId).ToList();
                var postInTopicModels = Mapper.Map<List<PostInTopicModel>>(postInTopic);
                return postInTopicModels;
            }
        }

        public int GetLikeForPostInTopic(int postInTopicId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var postLike = unitOfWork.PostInTopics.GetById(postInTopicId);
                postLike.Like++;
                unitOfWork.PostInTopics.Update(postLike);
                return postLike.Like;
            }
        }

        public int GetDislikeForPostInTopic(int postInTopicId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var postLike = unitOfWork.PostInTopics.GetById(postInTopicId);
                postLike.Like--;
                unitOfWork.PostInTopics.Update(postLike);
                return postLike.Like;
            }
        }

        public PostInTopicModel CreatePostInTopicAjax(PostInTopicRequestModel model, User currentUser)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var postInTopic = Mapper.Map<PostInTopic>(model);
                postInTopic.AuthorId = currentUser.Id;
                postInTopic.DateOfPublication = DateTime.Now;
                unitOfWork.PostInTopics.Create(postInTopic);
                postInTopic.Author = currentUser;
                return Mapper.Map<PostInTopicModel>(postInTopic);
            }
        }
    }
}
