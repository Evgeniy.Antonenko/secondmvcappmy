﻿using System.Collections.Generic;
using System.Linq;
using SecondMVCAppMy.Models.PostInTopics;

namespace SecondMVCAppMy.Services.PostInTopics
{
    public static class PostInTopicServiceExtensions
    {
        public static PostInTopicSortAndPageModel Sort(this List<PostInTopicModel> topicModels, PostInTopicSortState sortState)
        {
            var postInTopicSortAndPageModel = new PostInTopicSortAndPageModel()
            {
                DateOfPublicationSort = sortState == PostInTopicSortState.DateOfPublicationAsc ? PostInTopicSortState.DateOfPublicationDesc : PostInTopicSortState.DateOfPublicationAsc,
                AuthorSort = sortState == PostInTopicSortState.AuthorAsc ? PostInTopicSortState.AuthorDesc : PostInTopicSortState.AuthorAsc,
                PostInTopicContentSort = sortState == PostInTopicSortState.PostInTopicContentAsc ? PostInTopicSortState.PostInTopicContentDesc : PostInTopicSortState.PostInTopicContentAsc
            };

            switch (sortState)
            {
                case PostInTopicSortState.DateOfPublicationAsc:
                    topicModels = topicModels.OrderBy(t => t.DateOfPublication).ToList();
                    break;
                case PostInTopicSortState.DateOfPublicationDesc:
                    topicModels = topicModels.OrderByDescending(t => t.DateOfPublication).ToList();
                    break;
                case PostInTopicSortState.AuthorAsc:
                    topicModels = topicModels.OrderBy(t => t.Author).ToList();
                    break;
                case PostInTopicSortState.AuthorDesc:
                    topicModels = topicModels.OrderByDescending(t => t.Author).ToList();
                    break;
                case PostInTopicSortState.PostInTopicContentAsc:
                    topicModels = topicModels.OrderBy(t => t.PostInTopicContent).ToList();
                    break;
                case PostInTopicSortState.PostInTopicContentDesc:
                    topicModels = topicModels.OrderByDescending(t => t.PostInTopicContent).ToList();
                    break;
            }

            postInTopicSortAndPageModel.PostInTopics = topicModels;
            return postInTopicSortAndPageModel;
        }
    }
}
