﻿using System.Collections.Generic;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.Models.PostInTopics;

namespace SecondMVCAppMy.Services.PostInTopics.Contracts
{
    public interface IPostInTopicService
    {
        PostInTopicSortAndPageModel GetPostInTopicSortPageModel(int topicId, PostInTopicSortAndPageModel model, PostInTopicSortState sortState);
        List<PostInTopicModel> GetPostInTopicList(int topicId);
        PostInTopicModel CreatePostInTopicAjax(PostInTopicRequestModel model, User currentUser);
        int GetLikeForPostInTopic(int postInTopicId);
        int GetDislikeForPostInTopic(int postInTopicId);
    }
}
