﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SecondMVCAppMy.DAL;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.Models.Topics;
using SecondMVCAppMy.Services.Topics.Contracts;

namespace SecondMVCAppMy.Services.Topics
{
    public class TopicService : ITopicService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        
        public TopicService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public TopicFilterAndSortModel GetTopicFilterSortPageModel(int userId,
            TopicFilterAndSortModel model, TopicSortState sortState)
        {
            
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<PostInTopic> postInTopics = unitOfWork.PostInTopics.GetAllPostsWithAuthorsAndTopics().ToList();

                IEnumerable<PostInTopic> authorPostInTopics = unitOfWork.PostInTopics.GetAllPostsWithAuthorsAndTopics().Where(p => p.AuthorId == userId).ToList();
                
                IEnumerable<Topic> topics = unitOfWork.Topics.GetAllTopicsWithAuthors().ToList();

                topics = topics
                    .BySearchKey(model.SearchKey)
                    .ByAuthorName(model.Author)
                    .ByDateFrom(model.DateFrom)
                    .ByDateTo(model.DateTo);

                List<TopicModel> topicModels = Mapper.Map<List<TopicModel>>(topics);
                foreach (var topicModel in topicModels)
                {
                    topicModel.QtyUserPostsInTopic = authorPostInTopics.Where(a => a.TopicId == topicModel.Id).Count();
                }

                var topicFilterAndSortModel = topicModels.Sort(sortState);

                int pageSize = 3;
                int count = topics.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                TopicPageModel topicPageModel = new TopicPageModel(count, page, pageSize);
                topicFilterAndSortModel.Topics = topicFilterAndSortModel.Topics.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                topicFilterAndSortModel.Author = model.Author;
                topicFilterAndSortModel.SearchKey = model.SearchKey;
                topicFilterAndSortModel.DateFrom = model.DateFrom;
                topicFilterAndSortModel.DateTo = model.DateTo;
                topicFilterAndSortModel.TopicSortState = sortState;
                topicFilterAndSortModel.TopicPageModel = topicPageModel;
                topicFilterAndSortModel.Page = page;
                
                return topicFilterAndSortModel;
            }
        }
        
        public TopicModel CreateTopicAjax(TopicRequestModel model, User currentUser)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topic = Mapper.Map<Topic>(model);
                topic.AuthorId = currentUser.Id;
                topic.DateOfCreation = DateTime.Now;
                unitOfWork.Topics.Create(topic);
                topic.Author = currentUser;
                return Mapper.Map<TopicModel>(topic);
            }
        }

        public int GetLikeTopic(int topicId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topicLike = unitOfWork.Topics.GetById(topicId);
                topicLike.Likes++;
                unitOfWork.Topics.Update(topicLike);
                return topicLike.Likes;
            }
        }

        public int GetDislikeTopic(int topicId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var topicDislike = unitOfWork.Topics.GetById(topicId);
                topicDislike.Dislikes++;
                unitOfWork.Topics.Update(topicDislike);
                return topicDislike.Dislikes;
            }
        }
    }
}
