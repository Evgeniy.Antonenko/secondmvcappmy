﻿using System;
using System.Collections.Generic;
using System.Linq;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.Models.Topics;

namespace SecondMVCAppMy.Services.Topics
{
    public static class TopicServiceExtensions
    {
        public static IEnumerable<Topic> BySearchKey(this IEnumerable<Topic> topics, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                topics = topics.Where(t => t.TopicTitle.Contains(searchKey) || t.TopicContent.Contains(searchKey));

            return topics;
        }

        public static IEnumerable<Topic> ByAuthorName(this IEnumerable<Topic> topics, string authorName)
        {
            if (!string.IsNullOrWhiteSpace(authorName))
                topics = topics.Where(t => t.Author.UserName.Contains(authorName));

            return topics;
        }

        public static IEnumerable<Topic> ByDateFrom(this IEnumerable<Topic> topics, DateTime? dateFrom)
        {
            if (dateFrom.HasValue)
                topics = topics.Where(t => t.DateOfCreation >= dateFrom.Value);

            return topics;
        }

        public static IEnumerable<Topic> ByDateTo(this IEnumerable<Topic> topics, DateTime? dateTo)
        {
            if (dateTo.HasValue)
                topics = topics.Where(t => t.DateOfCreation <= dateTo.Value);

            return topics;
        }

        public static TopicFilterAndSortModel Sort(this List<TopicModel> topicModels, TopicSortState sortState)
        {
            var topicFilterAndSortModel = new TopicFilterAndSortModel()
            {
                DateOfCreationSort = sortState == TopicSortState.DateOfCreationAsc ? TopicSortState.DateOfCreationDesc : TopicSortState.DateOfCreationAsc,
                AuthorSort = sortState == TopicSortState.AuthorAsc ? TopicSortState.DateOfCreationDesc : TopicSortState.AuthorAsc,
                TopicTitleSort = sortState == TopicSortState.TopicTitleAsc ? TopicSortState.TopicTitleDesc : TopicSortState.TopicTitleAsc,
                TopicContentSort = sortState == TopicSortState.TopicContentAsc ? TopicSortState.TopicContentDesc : TopicSortState.TopicContentAsc
            };

            switch (sortState)
            {
                case TopicSortState.DateOfCreationAsc:
                    topicModels = topicModels.OrderBy(t => t.DateOfCreation).ToList();
                    break;
                case TopicSortState.DateOfCreationDesc:
                    topicModels = topicModels.OrderByDescending(t => t.DateOfCreation).ToList();
                    break;
                case TopicSortState.AuthorAsc:
                    topicModels = topicModels.OrderBy(t => t.Author).ToList();
                    break;
                case TopicSortState.AuthorDesc:
                    topicModels = topicModels.OrderByDescending(t => t.Author).ToList();
                    break;
                case TopicSortState.TopicTitleAsc:
                    topicModels = topicModels.OrderBy(t => t.TopicTitle).ToList();
                    break;
                case TopicSortState.TopicTitleDesc:
                    topicModels = topicModels.OrderByDescending(t => t.TopicTitle).ToList();
                    break;
                case TopicSortState.TopicContentAsc:
                    topicModels = topicModels.OrderBy(t => t.TopicContent).ToList();
                    break;
                case TopicSortState.TopicContentDesc:
                    topicModels = topicModels.OrderByDescending(t => t.TopicContent).ToList();
                    break;
            }

            topicFilterAndSortModel.Topics = topicModels;
            return topicFilterAndSortModel;
        }
    }
}
