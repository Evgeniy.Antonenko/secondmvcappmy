﻿using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.Models.Topics;

namespace SecondMVCAppMy.Services.Topics.Contracts
{
    public interface ITopicService
    {
        public TopicFilterAndSortModel GetTopicFilterSortPageModel(int userId, TopicFilterAndSortModel model, TopicSortState sortState);
        
        TopicModel CreateTopicAjax(TopicRequestModel model, User currentUser);

        int GetLikeTopic(int topicId);

        int GetDislikeTopic(int topicId);
    }
}
