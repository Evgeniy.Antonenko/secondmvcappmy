﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SecondMVCAppMy.Models.Topics
{
    public class TopicRequestModel
    {
        [Required]
        [JsonProperty(propertyName: "topicTitle")]
        [Display(Name = "Заголовок темы")]
        public string TopicTitle { get; set; }

        [Required]
        [JsonProperty(propertyName: "topicContent")]
        public string TopicContent { get; set; }
    }
}
