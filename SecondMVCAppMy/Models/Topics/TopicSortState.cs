﻿
namespace SecondMVCAppMy.Models.Topics
{
    public enum TopicSortState
    {
        DateOfCreationAsc,
        DateOfCreationDesc,
        AuthorAsc,
        AuthorDesc,
        TopicTitleAsc,
        TopicTitleDesc,
        TopicContentAsc,
        TopicContentDesc
    }
}
