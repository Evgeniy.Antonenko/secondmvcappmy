﻿using System.ComponentModel.DataAnnotations;

namespace SecondMVCAppMy.Models.Topics
{
    public class TopicModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата создания темы")]
        public string DateOfCreation { get; set; }

        public int AuthorId { get; set; }

        [Display(Name = "Автор темы")]
        public string Author { get; set; }

        [Display(Name = "Заголовок темы")]
        public string TopicTitle { get; set; }

        [Display(Name = "Заголовок темы")]
        public string TopicContent { get; set; }

        [Display(Name = "Кол-во сообщений")]
        public int PostsQty { get; set; }

        [Display(Name = "Кол-во Ваших сообщений")]
        public int QtyUserPostsInTopic { get; set; }

        [Display(Name = "Лайки")]
        public int Likes { get; set; }

        [Display(Name = "Дизлайки")]
        public int Dislikes { get; set; }
    }
}
