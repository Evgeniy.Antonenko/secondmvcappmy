﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SecondMVCAppMy.Models.PostInTopics;

namespace SecondMVCAppMy.Models.Topics
{
    public class TopicFilterAndSortModel
    {
        [Display(Name = "По автору темы")]
        public string Author { get; set; }

        [Display(Name = "По ключевому слову")]
        public string SearchKey { get; set; }

        [Display(Name = "Дата от")]
        public DateTime? DateFrom { get; set; }

        [Display(Name = "Дата до")]
        public DateTime? DateTo { get; set; }
        
        public TopicModel TopicModel { get; set; }

        public List<TopicModel> Topics { get; set; }
        
        public TopicSortState DateOfCreationSort { get; set; }

        public TopicSortState AuthorSort { get; set; }
        
        public TopicSortState TopicTitleSort { get; set; }
        
        public TopicSortState TopicContentSort { get; set; }
        
        public TopicSortState TopicSortState { get; set; }
        
        public int? Page { get; set; }
        
        public TopicPageModel TopicPageModel { get; set; }

        public PostInTopicModel PostInTopicModel { get; set; }

        public List<PostInTopicModel> PostInTopics { get; set; }
    }
}
