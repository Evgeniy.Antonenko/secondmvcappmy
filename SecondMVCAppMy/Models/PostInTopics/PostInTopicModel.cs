﻿using System.ComponentModel.DataAnnotations;

namespace SecondMVCAppMy.Models.PostInTopics
{
    public class PostInTopicModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата публикации сообщения")]
        public string DateOfPublication { get; set; }

        [Display(Name = "Сообщение")]
        public string PostInTopicContent { get; set; }

        [Display(Name = "Автор сообщения")]
        public string Author { get; set; }

        [Display(Name = "Тема сообщения")]
        public string Topic { get; set; }

        [Display(Name = "Лайки")]
        public int Like { get; set; }

        public int TopicId { get; set; }

        public int AuthorId { get; set; }
    }
}
