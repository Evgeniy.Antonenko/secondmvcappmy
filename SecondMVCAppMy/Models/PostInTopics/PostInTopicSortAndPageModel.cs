﻿using System.Collections.Generic;
using SecondMVCAppMy.Models.Topics;

namespace SecondMVCAppMy.Models.PostInTopics
{
    public class PostInTopicSortAndPageModel
    {
        public TopicModel TopicModel { get; set; }

        public PostInTopicModel PostInTopicModel { get; set; }

        public List<PostInTopicModel> PostInTopics { get; set; }

        public PostInTopicSortState DateOfPublicationSort { get; set; }

        public PostInTopicSortState AuthorSort { get; set; }

        public PostInTopicSortState PostInTopicContentSort { get; set; }

        public PostInTopicSortState PostInTopicSortState { get; set; }

        public int? Page { get; set; }

        public PostInTopicPageModel PostInTopicPageModel { get; set; }
    }
}
