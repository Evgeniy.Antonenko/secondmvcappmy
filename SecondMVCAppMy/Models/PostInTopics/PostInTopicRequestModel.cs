﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SecondMVCAppMy.Models.PostInTopics
{
    public class PostInTopicRequestModel
    {
        [Required]
        [JsonProperty(propertyName: "postInTopicContent")]
        public string Comment { get; set; }

        [Required]
        [JsonProperty(propertyName: "topicId")]
        public int TopicId { get; set; }
    }
}
