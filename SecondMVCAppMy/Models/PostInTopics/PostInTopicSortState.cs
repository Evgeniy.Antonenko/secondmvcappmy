﻿
namespace SecondMVCAppMy.Models.PostInTopics
{
    public enum PostInTopicSortState
    {
        DateOfPublicationAsc,
        DateOfPublicationDesc,
        AuthorAsc,
        AuthorDesc,
        PostInTopicContentAsc,
        PostInTopicContentDesc
    }
}
