﻿using System;
using System.Linq;
using AutoMapper;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.Models.PostInTopics;
using SecondMVCAppMy.Models.Topics;

namespace SecondMVCAppMy
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateTopicToTopicModelMap();
            CreatePostInTopicToPostInTopicModelMap();
            CreatePostInTopicModelToPostInTopic();
            CreatePostInTopicRequestModelToPostInTopic();
            CreateTopicRequestModelToTopic();
        }

        private void CreateTopicToTopicModelMap()
        {
            CreateMap<Topic, TopicModel>()
                .ForMember(target => target.DateOfCreation,
                    src => src.MapFrom(t => t.DateOfCreation.ToString("F")))
                .ForMember(target => target.Author,
                    src => src.MapFrom(t => t.Author.UserName))
                .ForMember(target => target.PostsQty,
                    src => src.MapFrom(t => t.PostInTopic.Where(p => p.TopicId == t.Id).Count()));
        }
        
        private void CreatePostInTopicToPostInTopicModelMap()
        {
            CreateMap<PostInTopic, PostInTopicModel>()
                .ForMember(target => target.DateOfPublication,
                    src => src.MapFrom(p => p.DateOfPublication.ToString("F")))
                .ForMember(target => target.Author,
                    src => src.MapFrom(p => p.Author.UserName))
                .ForMember(target => target.Topic,
                    src => src.MapFrom(p => p.Topic.TopicTitle));
        }

        public void CreatePostInTopicModelToPostInTopic()
        {
            CreateMap<PostInTopicModel, PostInTopic>()
                .ForMember(target => target.DateOfPublication,
                    src => src.MapFrom(t => DateTime.Now));
        }

        private void CreatePostInTopicRequestModelToPostInTopic()
        {
            CreateMap<PostInTopicRequestModel, PostInTopic>()
                .ForMember(target => target.PostInTopicContent,
                    src => src.MapFrom(p => p.Comment))
                .ForMember(target => target.TopicId,
                    src => src.MapFrom(p => p.TopicId));
        }

        private void CreateTopicRequestModelToTopic()
        {
            CreateMap<TopicRequestModel, Topic>()
                .ForMember(target => target.TopicTitle,
                    src => src.MapFrom(p => p.TopicTitle))
                .ForMember(target => target.TopicContent,
                    src => src.MapFrom(p => p.TopicContent));
        }
    }
}
