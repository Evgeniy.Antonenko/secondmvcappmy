﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.Models.Topics;
using SecondMVCAppMy.Services.Topics.Contracts;

namespace SecondMVCAppMy.Controllers
{
    public class TopicController : Controller
    {
        private readonly ITopicService _topicService;
        private readonly UserManager<User> _userManager;

        public TopicController(ITopicService topicService, UserManager<User> userManager)
        {
            if (topicService == null)
                throw new ArgumentNullException(nameof(topicService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));


            _topicService = topicService;
            _userManager = userManager;
        }
        
        [HttpGet]
        public async Task<IActionResult> Index(TopicFilterAndSortModel model, TopicSortState sortState = TopicSortState.DateOfCreationDesc)
        {
            try
            {
                int userId = 0;
                User currentUser = await _userManager.GetUserAsync(User);
                userId = currentUser == null ? 0 : currentUser.Id;
                    
                var topics = _topicService.GetTopicFilterSortPageModel(userId, model, sortState);
                return View(topics);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateTopicAjax(TopicRequestModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                var topicModel = _topicService.CreateTopicAjax(model, currentUser);

                return Ok(topicModel);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult LikeForTopicAjax(int? topicId)
        {
            if (!topicId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(topicId));
            try
            {
                var likes = _topicService.GetLikeTopic(topicId.Value);
                return Ok(likes);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult DisLikeForTopicAjax(int? topicId)
        {

            if (!topicId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(topicId));
            try
            {
                var dislikes = _topicService.GetDislikeTopic(topicId.Value);
                return Ok(dislikes);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
