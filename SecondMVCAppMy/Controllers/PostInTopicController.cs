﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.Models.PostInTopics;
using SecondMVCAppMy.Services.PostInTopics.Contracts;

namespace SecondMVCAppMy.Controllers
{
    public class PostInTopicController : Controller
    {
        private readonly IPostInTopicService _postInTopicService;
        private readonly UserManager<User> _userManager;

        public PostInTopicController(IPostInTopicService postInTopicService, UserManager<User> userManager)
        {
            if (postInTopicService == null)
                throw new ArgumentNullException(nameof(postInTopicService));
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _postInTopicService = postInTopicService;
            _userManager = userManager;
        }

        [Authorize]
        [HttpGet]
        public IActionResult Index(int? topicId, PostInTopicSortAndPageModel model, PostInTopicSortState sortState = PostInTopicSortState.DateOfPublicationAsc)
        {
            if (!topicId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(topicId));
            try
            {
                var postInTopics = _postInTopicService.GetPostInTopicSortPageModel(topicId.Value, model, sortState);
                return View(postInTopics);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult LikeForPostInTopic(int? postInTopicId)
        {
            if (!postInTopicId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(postInTopicId));
            try
            {
                var likes = _postInTopicService.GetLikeForPostInTopic(postInTopicId.Value);
                return Ok(likes);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult DisLikeForPostInTopic(int? postInTopicId)
        {

            if (!postInTopicId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(postInTopicId));
            try
            {
                var dislikes = _postInTopicService.GetDislikeForPostInTopic(postInTopicId.Value);
                return Ok(dislikes);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreatePostInTopicAjax(PostInTopicRequestModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                var postInTopicModel = _postInTopicService.CreatePostInTopicAjax(model, currentUser);
                
                return Ok(postInTopicModel);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
