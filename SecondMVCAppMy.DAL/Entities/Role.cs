﻿using Microsoft.AspNetCore.Identity;

namespace SecondMVCAppMy.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}
