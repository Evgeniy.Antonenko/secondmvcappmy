﻿using System;
using System.Collections.Generic;

namespace SecondMVCAppMy.DAL.Entities
{
    public class Topic : IEntity
    {
        public int Id { get; set; }

        public DateTime DateOfCreation { get; set; }

        public string TopicTitle { get; set; }

        public string TopicContent { get; set; }

        public int AuthorId { get; set; }

        public User Author { get; set; }

        public ICollection<PostInTopic> PostInTopic { get; set; }

        public int Likes { get; set; }

        public int Dislikes { get; set; }
    }
}
