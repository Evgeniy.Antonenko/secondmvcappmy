﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace SecondMVCAppMy.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public ICollection<Topic> Topic { get; set; }

        public ICollection<PostInTopic> PostInTopic { get; set; }
    }
}
