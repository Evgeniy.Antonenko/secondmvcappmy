﻿using System;

namespace SecondMVCAppMy.DAL.Entities
{
    public class PostInTopic : IEntity
    {
        public int Id { get; set; }

        public DateTime DateOfPublication { get; set; }

        public string PostInTopicContent { get; set; }

        public int AuthorId { get; set; }
        
        public User Author { get; set; }

        public int TopicId { get; set; }

        public Topic Topic { get; set; }

        public int Like { get; set; }
    }
}
