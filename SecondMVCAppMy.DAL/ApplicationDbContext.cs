﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.DAL.EntitiesConfiguration.Contracts;

namespace SecondMVCAppMy.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;

        public DbSet<Topic> Topics { get; set; }
        public DbSet<PostInTopic> PostInTopics { get; set; }

        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity(_entityConfigurationsContainer.TopicConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.PostInTopicConfiguration.ProvideConfigurationAction());
        }
    }
}
