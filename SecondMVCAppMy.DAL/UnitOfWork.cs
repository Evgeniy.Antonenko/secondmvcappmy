﻿using System;
using SecondMVCAppMy.DAL.Repositories;
using SecondMVCAppMy.DAL.Repositories.Contracts;

namespace SecondMVCAppMy.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public ITopicRepository Topics { get; set; }
        public IPostInTopicRepository PostInTopics { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Topics = new TopicRepository(context);
            PostInTopics = new PostInTopicRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
