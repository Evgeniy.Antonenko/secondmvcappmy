﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SecondMVCAppMy.DAL.Migrations
{
    public partial class AddFieldLikeinPostInTopic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Like",
                table: "PostInTopics",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Like",
                table: "PostInTopics");
        }
    }
}
