﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SecondMVCAppMy.DAL.Migrations
{
    public partial class AddLikesAndDislikesField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Dislikes",
                table: "Topics",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Likes",
                table: "Topics",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Dislikes",
                table: "Topics");

            migrationBuilder.DropColumn(
                name: "Likes",
                table: "Topics");
        }
    }
}
