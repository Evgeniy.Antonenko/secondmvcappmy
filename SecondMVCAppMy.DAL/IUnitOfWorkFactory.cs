﻿
namespace SecondMVCAppMy.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
