﻿using SecondMVCAppMy.DAL.Entities;

namespace SecondMVCAppMy.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Topic> TopicConfiguration { get; }
        IEntityConfiguration<PostInTopic> PostInTopicConfiguration { get; }
    }
}
