﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SecondMVCAppMy.DAL.Entities;

namespace SecondMVCAppMy.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
