﻿using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.DAL.EntitiesConfiguration.Contracts;

namespace SecondMVCAppMy.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Topic> TopicConfiguration { get; }
        public IEntityConfiguration<PostInTopic> PostInTopicConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            TopicConfiguration = new TopicConfiguration();
            PostInTopicConfiguration = new PostInTopicConfiguration();
        }
    }
}
