﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SecondMVCAppMy.DAL.Entities;

namespace SecondMVCAppMy.DAL.EntitiesConfiguration
{
    public class PostInTopicConfiguration : BaseEntityConfiguration<PostInTopic>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<PostInTopic> builder)
        {
            builder
                .Property(p => p.PostInTopicContent)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(p => p.DateOfPublication)
                .IsRequired();
            builder
                .HasIndex(p => p.Id)
                .IsUnique();
        }
        
        protected override void ConfigureForeignKeys(EntityTypeBuilder<PostInTopic> builder)
        {
            builder
                .HasOne(p => p.Author)
                .WithMany(p => p.PostInTopic)
                .HasForeignKey(t => t.AuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasOne(p => p.Topic)
                .WithMany(p => p.PostInTopic)
                .HasForeignKey(p => p.TopicId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
