﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SecondMVCAppMy.DAL.Entities;

namespace SecondMVCAppMy.DAL.EntitiesConfiguration
{
    public class TopicConfiguration : BaseEntityConfiguration<Topic>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Topic> builder)
        {
            builder
                .Property(t => t.TopicTitle)
                .HasMaxLength(150)
                .IsRequired();
            builder
                .Property(t => t.TopicContent)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(t => t.DateOfCreation)
                .IsRequired();
            builder
                .HasIndex(t => t.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Topic> builder)
        {
            builder
                .HasOne(t => t.Author)
                .WithMany(t => t.Topic)
                .HasForeignKey(t => t.AuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasMany(t => t.PostInTopic)
                .WithOne(t => t.Topic)
                .HasForeignKey(t => t.TopicId)
                .IsRequired();
        }
    }
}
