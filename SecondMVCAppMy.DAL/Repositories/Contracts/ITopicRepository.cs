﻿using System.Collections.Generic;
using SecondMVCAppMy.DAL.Entities;

namespace SecondMVCAppMy.DAL.Repositories.Contracts
{
    public interface ITopicRepository : IRepository<Topic>
    {
        IEnumerable<Topic> GetAllTopicsWithAuthors();
        Topic GetByIdFullTopic(int id);
    }
}
