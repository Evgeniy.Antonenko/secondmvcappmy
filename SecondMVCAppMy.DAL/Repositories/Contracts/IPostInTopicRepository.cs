﻿using System.Collections.Generic;
using SecondMVCAppMy.DAL.Entities;

namespace SecondMVCAppMy.DAL.Repositories.Contracts
{
    public interface IPostInTopicRepository : IRepository<PostInTopic>
    {
        IEnumerable<PostInTopic> GetAllPostsWithAuthorsAndTopics();
        IEnumerable<PostInTopic> GetPostsWithAutorsAndTopicByTopics(int topicId);
    }
}
