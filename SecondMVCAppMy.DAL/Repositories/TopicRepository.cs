﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.DAL.Repositories.Contracts;

namespace SecondMVCAppMy.DAL.Repositories
{
    public class TopicRepository : Repository<Topic>, ITopicRepository
    {
        public TopicRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Topics;
        }

        public IEnumerable<Topic> GetAllTopicsWithAuthors()
        {
            return entities.Include(t => t.Author).ToList();
        }

        public Topic GetByIdFullTopic(int id)
        {
            return GetAllTopicsWithAuthors().FirstOrDefault(p => p.Id == id);
        }
    }
}
