﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SecondMVCAppMy.DAL.Entities;
using SecondMVCAppMy.DAL.Repositories.Contracts;

namespace SecondMVCAppMy.DAL.Repositories
{
    public class PostInTopicRepository : Repository<PostInTopic>, IPostInTopicRepository
    {
        public PostInTopicRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.PostInTopics;
        }

        public IEnumerable<PostInTopic> GetAllPostsWithAuthorsAndTopics()
        {
            return entities.Include(p => p.Author).Include(p => p.Topic);
        }

        public IEnumerable<PostInTopic> GetPostsWithAutorsAndTopicByTopics(int topicId)
        {
            return GetAllPostsWithAuthorsAndTopics().Where(p => p.TopicId == topicId);
        }
    }
}
